import 'package:flutter/material.dart';
import 'package:flutter_application_1/widgets/info_card.dart';

// our data
const url = "Lethabo Mokgokoloshi";
const email = "lethabo1.mokg@gmail.com";
const phone = "067*******"; // not real number :)
const location = "Gauteng, Johannesburg";

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 243, 147, 200),
      appBar: AppBar(
        elevation: 1,
        backgroundColor: const Color.fromARGB(255, 11, 70, 75),
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          onPressed: () => {Navigator.of(context).pop()},
        ),
      ),
      body: SafeArea(
        minimum: const EdgeInsets.only(top: 10),
        child: Column(
          children: <Widget>[
            const CircleAvatar(
              radius: 60,
              backgroundImage: AssetImage('assets/leth.jpg'),
            ),
            const Text(
              "Coding Books",
              style: TextStyle(
                fontSize: 40.0,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontFamily: "Robo Mono",
              ),
            ),
            const Text(
              "Flutter Developer",
              style: TextStyle(
                  fontSize: 30,
                  color: Color.fromARGB(255, 3, 58, 85),
                  letterSpacing: 2.5,
                  fontWeight: FontWeight.bold,
                  fontFamily: "Robo Mono"),
            ),
            const SizedBox(
              height: 20,
              width: 200,
              child: Divider(
                color: Colors.white,
              ),
            ),
            // we will be creating a new widget name info carrd
            InfoCard(text: phone, icon: Icons.phone, onPressed: () async {}),
            InfoCard(text: url, icon: Icons.web, onPressed: () async {}),
            InfoCard(
                text: location,
                icon: Icons.location_city,
                onPressed: () async {}),
            InfoCard(text: email, icon: Icons.email, onPressed: () async {}),
            Container(
              padding: const EdgeInsets.only(top: 100),
              height: 200,
              decoration: const BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/code2.png"),
                    fit: BoxFit.fitHeight),
              ),
            )
          ],
        ),
      ),
    );
  }
}
